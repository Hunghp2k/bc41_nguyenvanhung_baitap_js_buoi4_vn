/**bài 1
 * input: nguoi dung nhap vao ngay thang nam
 * progress: ktr co phai la nam Nhuan hay khong
 *           tinh truong hợp đặc biết tháng 2 (nhuận or không) && tháng 12
 * output: in ra ngày tháng năm tiếp theo;
 *          in ra ngay thang nam truoc do 
 */

document.getElementById('btn_bai1').onclick = function () {
    var date = document.getElementById('date').value * 1;
    var month = document.getElementById('month').value * 1;
    var year = document.getElementById('year').value * 1;
    var namNhuan = '';
    console.log(date, month, year);
    if (year % 4 == 0 & year % 100 != 0 || year % 400 == 0) {
        namNhuan = "namNhuan";
    } else {
        namNhuan = 'khongNhuan'
    }

    console.log(year);
    var soNgayTrongThang = 0;
    switch (month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            soNgayTrongThang = 31;
        } break;
        case 2: {
            if (namNhuan == 'namNhuan') {
                soNgayTrongThang = 29;
            } else {
                soNgayTrongThang = 28;
            }
        } break
        case 4: case 6: case 9: case 11: {
            soNgayTrongThang = 30;
        } break;
        default: {
            soNgayTrongThang = -1;
        }
    }
    console.log(soNgayTrongThang);

    var dTT;
    var mTT;
    var yTT;
    console.log(year > 0 && month > 0 && month < 13 && date > 0 && date <= soNgayTrongThang);
    if (year > 0 && month > 0 && month < 13 && date > 0 && date <= soNgayTrongThang) {
        // dTT = date +1; 
        if (month == 2) {
            if (namNhuan = 'namNhuan' && date == 29) {
                dTT = date = 1;
                mTT = month + 1;
                yTT = year;
            } else {
                dTT = date = 1;
                mTT = month + 1;
                yTT = year;
            }
        } else if (month == 12) {
            if (date < soNgayTrongThang) {
                dTT = date + 1;
                mTT = month;
                yTT = year;
            } else {
                dTT = date = 1;
                mTT = month = 1;
                yTT = year + 1;
            }
        } else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10) {
            if (date == 31) {
                dTT = 1;
                mTT = month + 1;
                yTT = year;
            } else {
                dTT = date + 1;
                mTT = month;
                yTT = year;
            }
        } else {
            if (date == 30) {
                dTT = 1;
                mTT = month + 1;
                yTT = year
            } else {
                dTT = date + 1;
                mTT = month;
                yTT = year;
            }
        }
    }
    document.getElementById('result_1').innerHTML = `Ngày tháng năm tiếp theo: ${dTT} - ${mTT} - ${yTT}`

    var dTD;
    var mTD;
    var yTD;
    if (year > 0 && month > 0 && month < 13 && date > 0 && date <= soNgayTrongThang) {
        if (month == 3) {
            if (namNhuan = 'namNhuan' && date == 1) {
                dTD = 29;
                mTD = 2;
                yTD = year;
            } else {
                dTD = 28;
                mTD = 2;
                yTD = year;
            }
        } else if (month == 1) {
            if (date == 1) {
                dTD = 31;
                mTD = 12;
                yTD = year - 1;
            } else {
                dTD = date - 1;
                mTD = month;
                yTD = year
            }
        } else if (date == 1) {
            if (month == 2 || month == 4 || month == 6 || month == 8 || month == 9 || month == 11) {
                dTD = 31;
                mTD = month - 1;
                yTD = year;
            } else {
                dTD = 30;
                mTD = month - 1;
                yTD = year;
            }
        } else {
            dTD = date - 1;
            mTD = month;
            yTD = year
        }
    }

    document.getElementById('result_1_TD').innerHTML = `Ngày tháng năm trước đó: ${dTD} - ${mTD} - ${yTD}`
}



/**bài 2
 * Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng
của năm nhuận).
input: lấy giá trị người dùng nhập vào
progress: year && month > 0 && month < 13   
          Test value year === namNham thì tháng 2 có 29 date
               value year !== namNhan thi thang 2 co 28 date
          month 1,3,5,7,8,10,12 => 31 date
          month 2,4,6,9,11 ==> 30 date.
output: month user nhập có bnh ngày 
 */


document.getElementById('btn_bai2').onclick = function () {
    var month_2 = document.getElementById('month_2').value * 1;
    var year_2 = document.getElementById('year_2').value * 1;
    var nhuan = '';
    if (year_2 % 4 == 0 && year_2 % 100 != 0 || year_2 % 400 == 0) {
        nhuan = true;
    } else {
        nhuan = false;
    }
    var tongSoNgay = 0
    switch (month_2) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            tongSoNgay = 31;
        } break;
        case 2: {
            if (nhuan == true) {
                tongSoNgay = 29;
            } else {
                tongSoNgay = 28;
            }
        } break;
        case 4: case 6: case 9: case 11: {
            tongSoNgay = 30;
        } break;
        default: {
            tongSoNgay = -1;
        }
    }
    document.getElementById('result_bai2').innerHTML = `Số ngày của tháng = ${tongSoNgay}`
}



/**
 * bài 3: Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó. 
 * input: lấy value user
 * progress: lấy giá trị từng hàng: trăm chục đơn vị
 * ==> cách đọc rồi cộng lại với nhau
 * input: nhập vào cách đọc của user
 */

document.getElementById('btn_bai3').onclick = function () {
    var input_bai3 = document.getElementById('input_bai3').value * 1;
    var hangTram = Math.floor(input_bai3 / 100);
    var hangChuc = Math.floor((input_bai3 / 10) % 10);
    var donVi = Math.floor(input_bai3 % 10)
    if (input_bai3 < 100 || input_bai3 > 999) {
        document.getElementById('result_bai3').innerHTML = 'xin mời nhập vào số có 3 chữ số';
    } else {
        switch (hangTram) {
            case 1: {
                read_Tram = "Một trăm";
            } break;
            case 2: {
                read_Tram = "Hai trăm";
            } break;
            case 3: {
                read_Tram = "Ba trăm";
            } break;
            case 4: {
                read_Tram = "Bốn trăm";
            } break;
            case 5: {
                read_Tram = "Năm trăm";
            } break;
            case 6: {
                read_Tram = "Sáu trăm";
            } break;
            case 7: {
                read_Tram = "Bảy trăm";
            } break;
            case 8: {
                read_Tram = "Tám trăm";
            } break;
            case 9: {
                read_Tram = "Chín trăm";
            } break;
        }
    }
    var read_Chuc = '';
    if (hangChuc % 10 == 0 && donVi != 0) {
        read_Chuc = 'lẻ';
    } else {
        switch (hangChuc) {
            case 1: {
                read_Chuc = "mười";
            } break;
            case 2: {
                read_Chuc = "hai mươi ";
            } break;
            case 3: {
                read_Chuc = "Ba mươi ";
            } break;
            case 4: {
                read_Chuc = "Bốn mươi ";
            } break;
            case 5: {
                read_Chuc = "Năm mươi ";
            } break;
            case 6: {
                read_Chuc = "Sáu mươi ";
            } break;
            case 7: {
                read_Chuc = "Bảy mươi ";
            } break;
            case 8: {
                read_Chuc = "Tám mươi ";
            } break;
            case 9: {
                read_Chuc = "Chín mươi ";
            } break;
        }
    }
    var read_DV = '';
    switch (donVi) {
        case 1: {
            read_DV = "một";
        } break;
        case 2: {
            read_DV = "hai";
        } break;
        case 3: {
            read_DV = "Ba";
        } break;
        case 4: {
            read_DV = "Bốn";
        } break;
        case 5: {
            read_DV = "Năm";
        } break;
        case 6: {
            read_DV = "Sáu";
        } break;
        case 7: {
            read_DV = "Bảy";
        } break;
        case 8: {
            read_DV = "Tám";
        } break;
        case 9: {
            read_DV = "Chín";
        } break;
    }
    document.getElementById('result_bai3').innerHTML = `${read_Tram} ${read_Chuc} ${read_DV}`

}

//bài 4
/*input: lấy giá trị tọa độ của 3 sv và trường học nhập vào
 * progress: dùng công thức tính ra độ dài của từng sinh viên
 *          so sánh độ dài của từng sinh viên => sinh viên xa trường Nhất
 * output: Xuất ra name của Sv xa trường nhất
*/

document.getElementById('btn_bai4').onclick = function () {
    var name1 = document.getElementById('sinhvien1').value;
    var name2 = document.getElementById('sinhvien2').value;
    var name3 = document.getElementById('sinhvien3').value;
    var x1 = document.getElementById('x1').value * 1;
    var x2 = document.getElementById('x2').value * 1;
    var x3 = document.getElementById('x3').value * 1;
    var x4 = document.getElementById('x4').value * 1;
    var y1 = document.getElementById('y1').value * 1;
    var y2 = document.getElementById('y2').value * 1;
    var y3 = document.getElementById('y3').value * 1;
    var y4 = document.getElementById('y4').value * 1;
    var test_4;
    if (x1 > 0 && x2 > 0 && x3 > 0 && x4 > 0 && y1 > 0 && y2 > 0 && y3 > 0 && y4 > 0) {
        test_4 = true;
    } else {
        alert('Tọa độ bạn nhập không đúng');
    }
    var distance_1;
    distance_1 = Math.sqrt((x4 - x1) * (x4 - x1) + (y4 - y1) * (y4 - y1));
    var distance_2;
    distance_2 = Math.sqrt((x4 - x2) * (x4 - x2) + (y4 - y2) * (y4 - y2));
    var distance_3;
    distance_3 = Math.sqrt((x4 - x3) * (x4 - x3) + (y4 - y3) * (y4 - y3));
    if (distance_1 === distance_2 === distance_3) {
        document.getElementById('result__bai4').innerHTML = `Khoảng đường của 3 sinh viên ngang nhau`
    } else {
        if (distance_1 > distance_2 && distance_1 > distance_3) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name1} xa nhất`
        } else if (distance_2 > distance_1 && distance_2 > distance_3) {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name2} xa nhất`
        } else {
            document.getElementById('result__bai4').innerHTML = `Sinh viên ${name3} xa nhất`
        }
    }
}














